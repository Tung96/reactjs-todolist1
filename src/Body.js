import React, { Component } from 'react';
import TodoList from './TodoList';
import AddTask from './AddTask';
import './body.css';


class Body extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tasks: [],
        }
       
    }
    componentDidMount() { 
        
    }

    setStatus = () => {
        this.setState({
            showAddForm: true
        })
        // console.log("them noi dung")
    }
    closeForm = () => {
        this.setState({
            showAddForm: false,
            showEditForm: false
        })
    }
    addTask = (name) => {
        this.state.tasks.push(name)
        console.log(this.state.tasks)
        this.forceUpdate()
        // console.log(this.state.tasks)
    }

    // deleteTask = () => {
    //     this.state.tasks.splice(name,1)

    // }
    // setTitle = (x) => {
    //     // let title = this.state.title
    //     // title = 'Title has change!'
    //     // this.setState({title: title})
    //     console.log();
    // }


    // remove(index){
    //     this.state.tanks.splice(index,1)
    //     this.setState(this.state)
    // }
    deleteTasks = (name) => {
        let tasks = this.state.tasks
        tasks = tasks.slice(0, 1)
        this.setState({ tasks: tasks})
        this.forceUpdate()
    }

    render() {
         if (this.state.showAddForm === true) {
            return (
                <AddTask addTask={this.addTask} closeForm={this.closeForm} />
            )
        } else {
            return (
                <div>
                    <div className="container body">
                        <button type="button" className="btn btn-outline-primary-body" onClick={this.setStatus} >Thêm nội dung</button>
                        <h2>Todo List</h2>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nội dung cần nhập</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td onClick={this.deleteTasks}>delete task</td></tr>
                                {
                                    this.state.tasks.map(function (name, index) {
                                        return  ( <TodoList name={name} callback={this.deleteTasks}/>)
                                    }.bind(this))
                                }
                            </tbody>
                        </table>
                    </div>
                </div> 
            );
        }
        
    }
}

export default Body;