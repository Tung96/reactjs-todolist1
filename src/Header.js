import React, { Component } from 'react';
import './header.css';
class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tasks: [],
            namehd: this.props.title || '',
            showAddForm: false,
        }
    }
    componentDidMount() { 
        
    }

    setTitle = (x) => {
        let title1 = this.state.namehd
        title1 = 'Title has change!'
        this.setState({namehd: title1})
    }

    changetitle = () => {
        let aaa = this.state.namehd
        aaa = this.props.title || ''
        this.setState({namehd: aaa})
    }



    render() {
        return (
            <div className="container">
                <div className='header'>
                {this.state.namehd}
                </div>
                <br/>
                <button type="button" className="btn btn-outline-primary-chan" onClick={this.setTitle} >Change title</button>
                <br/>
                <button type="button" className="btn btn-outline-primary-re" onClick={this.changetitle}>Trở lại</button>
            </div>
        );
        
    }
}

export default Header;