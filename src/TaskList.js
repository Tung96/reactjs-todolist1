import React, { Component } from 'react';
import AddTask from './AddTask';
import Header from './Header';
import Footer from './Footer';
import Body from './Body';


class TaskList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tasks: [],
            showAddForm: false,
            title: 'Header custom',
            name:'Đây là footer!'

        }
    }

    componentDidUpdate(prevProps) { 
        if (this.props.userID !== prevProps.userID) {
            this.fetchData(this.props.userID);
            }
        }

    setStatus = () => {
        this.setState({
            showAddForm: true
        })
    }
    closeForm = () => {
        this.setState({
            showAddForm: false,
            showEditForm: false
        })
    }
    addTask = (name) => {
        this.state.tasks.push(name)
        this.forceUpdate()
    }
    // setTitle = (x) => {
    //     let title = this.props.title
    //     title = 'Title has change!'
    //     this.setState({title: title1})
    // }


    
    
    render() {
        if (this.state.showAddForm === true) {
            return (
                <AddTask addTask={this.addTask} closeForm={this.closeForm} />
            )
        } else {
            return (
                <div>
                    <Header title={this.state.title} />
                    <br/>
                    <Body/>
                    <br/>
                    <Footer name={this.state.name} />
                </div> 
            );
        }
    }
}


export default TaskList;