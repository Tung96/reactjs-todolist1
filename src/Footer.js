import React, { Component } from 'react';

class Footer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tasks: [],
            title: this.props.title || '',
            showAddForm: false,
            name:'Đây là footer!',
        }
    }

    setFooter = (a) => {
        let changefoot = this.state.changefoot
        changefoot = 'thay đổi nội dung footer!'
        this.setState({name: changefoot})
        // console.log(a);
    }


    componentDidMount() { 
        
    }
    render() {
            return (
            <div className="container">
                <div className='footer'>{this.state.name}</div>
                <button type="button" className="btn btn-primary"  onClick={this.setFooter}>chuyển tab mới</button>
            </div>
            );
        
    }
}



export default Footer;